# About

Sass demo 5 mei 2021
## Usage

-   From the project directory enter the following commands:

```
$ npm i
$ npm start
```

## Technologies used
-   npm
-   webpack
    - loaders for css, fonts, etc.
    - plugin for HTML template
    - webpack-dev-server
